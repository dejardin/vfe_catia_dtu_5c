#!/bin/bash
now=`date +%s`
reply=""
declare -i fe_adapter=4
if [[ "x"$1 != "x" ]]; then
  fe_adapter=$1
fi
declare -i vfe=0
if [[ "x"$2 != "x" ]]; then
  vfe=$2
fi
declare -i ip
ip=$fe_adapter+16
echo "Mettre en route les basses tensions"
echo "Mettre en route la haute tension et regler a ~330 V"
echo "Charger le firmware dans le FPGA"
echo "Mettre le laser en attente"
echo "Allumer le géné HP"
while [[ $reply != "o" && $reply != "O" && $reply != "q" && $reply != "Q" ]]; do
  echo -n "Tout est prêt ? [o/n/q] "
  read reply
done
if [[ $reply == "q" || $reply == "Q" ]]; then
  exit 1
fi
declare -i retcode=1
declare -i ntry=0
echo $retcode $ntry
while [[ $retcode -ne 0 && $ntry -lt 10 ]]; do 
  ping -W 1 -c 1 10.0.0.$ip
  retcode=$?
  ((ntry++))
done
if [[ $ntry -eq 10 ]]; then
  echo "Impossible de pinguer la carte FE-adapter après 10 essais."
  echo "Vérifiez la connection et les alimentations des interfaces réseau."
  echo "Relancer le programme après vérification"
  exit 1
fi

reply="r"
while [[ $reply == "r" || $reply == "R" ]]; do
  echo "**********************************************"
  echo "Mesure du bruit sur les TIA3-G10"
  echo "**********************************************"
  bin/loc_daq.exe -vfe $fe_adapter -trigger_type 0 -nsample 28670 -dbg 0 -nevt 500 -n_calib 1 -soft_trigger 1 -negate_data 1 -signed_data 0 -input_span 0 2>&1 \
                  | tee /data/cms/ecal/fe/TB_2018/vfe_$vfe/ped.$now.log
  root -q -e '.L utils/ana_ped.C+' -e 'ana_ped("/data/cms/ecal/fe/TB_2018/test/ped_data.root",-1,3,1.75,1.) '
  mv /data/cms/ecal/fe/TB_2018/test/ped_data.root /data/cms/ecal/fe/TB_2018/vfe_$vfe/ped_data_TIA3_G10.$now.root
  mv /data/cms/ecal/fe/TB_2018/test/ped_data_analized.root /data/cms/ecal/fe/TB_2018/vfe_$vfe/ped_data_TIA3_G10.${now}_analized.root
  mv /data/cms/ecal/fe/TB_2018/test/ped_data_analized.txt  /data/cms/ecal/fe/TB_2018/vfe_$vfe/ped_data_TIA3_G10.${now}_analized.txt
  echo -n "On continue ou on recommence ? [c/r] "
  read reply
done

reply="r"
while [[ $reply == "r" || $reply == "R" ]]; do
  echo "**********************************************"
  echo "Mesure des gains sur les TIA3-G10"
  echo "**********************************************"
  bin/loc_daq.exe -vfe $fe_adapter -trigger_type 1 -nsample 2000 -dbg 0 -nevt 100 -n_calib 45 -calib_step -100 -calib_level 32500 -calib_width 1000 \
                  -soft_trigger 1 -negate_data 1 -signed_data 0 -input_span 0 2>&1 \
                  | tee -a /data/cms/ecal/fe/TB_2018/vfe_$vfe/TP.$now.log 
  root -q -e '.L utils/linearity.C' -e 'linearity("/data/cms/ecal/fe/TB_2018/test/TP_data.root",32500,-100,45,1,1200,1,1.75,1.)'
  mv /data/cms/ecal/fe/TB_2018/test/TP_data.root /data/cms/ecal/fe/TB_2018/vfe_$vfe/TP_data_TIA3_G10.$now.root
  mv /data/cms/ecal/fe/TB_2018/test/TP_data_analized.root /data/cms/ecal/fe/TB_2018/vfe_$vfe/TP_data_TIA3_G10.${now}_analized.root
  mv /data/cms/ecal/fe/TB_2018/test/TP_data_analized.txt  /data/cms/ecal/fe/TB_2018/vfe_$vfe/TP_data_TIA3_G10.${now}_analized.txt
  echo -n "On continue ou on recommence ? [c/r] "
  read reply
done

echo "Mettre en route le laser"
reply=""
while [[ $reply != "o" && $reply != "O" && $reply != "q" && $reply != "Q" ]]; do
  echo -n "OK ? [o/n/q] "
  read reply
done
if [[ $reply == "q" || $reply == "Q" ]]; then
  exit 2
fi
reply="r"
while [[ $reply == "r" || $reply == "R" ]]; do
  echo "**********************************************"
  echo "Mesure des fonctions de transfert sur les TIA3-G10"
  echo "**********************************************"
  bin/loc_daq.exe -vfe $fe_adapter -trigger_type 2 -nsample 200 -dbg 0 -nevt 10000 -n_calib 1 -self_trigger 1 -self_trigger_threshold 650 -self_trigger_mask 4 \
                  -soft_trigger 0 -negate_data 1 -signed_data 0 -input_span 0 2>&1 \
                  | tee -a /data/cms/ecal/fe/TB_2018/vfe_$vfe/laser.$now.log
  mv /data/cms/ecal/fe/TB_2018/test/laser_data.root /data/cms/ecal/fe/TB_2018/vfe_$vfe/laser_data_TIA3_G10.$now.root
  echo -n "On continue ou on recommence ? [c/r] "
  read reply
done

echo "Couper le laser"
echo "Couper la haute tension et attendre le retour à 0"
echo "Couper les basses tensions"
echo "Couper le géné HP"
echo "Vous pouvez récuperer la carte !"
echo "Liste des fichiers générés :"
echo "/data/cms/ecal/fe/TB_2018/vfe_$2/ped_data_TIA3_G10.$now.root"
echo "/data/cms/ecal/fe/TB_2018/vfe_$2_top/test/TP_data_TIA3_G10.$now.root"
echo "/data/cms/ecal/fe/TB_2018/vfe_$2_top/test/laser_data_TIA3_G10.$now.root"

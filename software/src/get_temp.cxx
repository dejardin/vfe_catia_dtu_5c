#include "uhal/uhal.hpp"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>
//#include <time.h>
#include <sys/time.h>

#define GENE_TRIGGER    (1<<0)
#define GENE_CALIB      (1<<1)
#define GENE_100HZ      (1<<2)
#define LED_ON          (1<<3)
#define CALIB_MODE      (1<<4)

#define RESET                  (1<<0)
#define TRIGGER_MODE           (1<<1)
#define SOFT_TRIGGER           (1<<2)
#define SELF_TRIGGER           (1<<3)
#define CLOCK_PHASE            (1<<4)
#define CLOCK_RESET            (1<<7)
#define SELF_TRIGGER_THRESHOLD (1<<8)
#define SELF_TRIGGER_MASK      (1<<22)
#define ADC_CALIB_MODE         (1<<27)
#define BOARD_SN               (1<<28)
#define SEQ_CLOCK_PHASE        (1<<0)
#define IO_CLOCK_PHASE         (1<<4)
#define REG_CLOCK_PHASE        (1<<8)
#define MEM_CLOCK_PHASE        (1<<12)

#define CAPTURE_START 1
#define CAPTURE_STOP  2
//#define SW_DAQ_DELAY 0x1800 // delay for laser with internal trigger
#define SW_DAQ_DELAY 0x0 // delay for laser with external trigger
#define HW_DAQ_DELAY 100 // Laser with external trigger
#define NSAMPLE_MAX 28672
#define MAX_PAYLOAD 1380
#define MAX_VFE     10
#define MAX_STEP    10000

#define DRP_WRb         (1<<31)
#define I2C_RWb         (1<<31)
#define I2C_LONG        (1<<30)
#define SPI_CATIA       (1<<26)
#define I2C_CATIA       (1<<25)
#define ADC_WRITE       (1<<24)

#define ADC_POWER_MODE  (0x08<<16) // ADC register to define the power mode (0=ON, 1==OFF, 2=Standby)
#define ADC_OMODE_REG   (0x14<<16) // ADC register to define Output mode of the ADC
#define ADC_ISPAN_REG   (0x18<<16) // ADC register to define input span of the ADC from 1.383V to 2.087V
#define ADC_CLK_DIVIDE  (0x0B<<16) // ADC register to define clock division of the ADC
#define ADC_OTEST_REG   (0x0D<<16) // ADC register to define test mode of the ADC

#define DELAY_INCREASE  (1<<5)     // Increase ADC signal delay by one tip
#define DELAY_RESET     (1<<6)     // Reset ADC signal delays

using namespace uhal;

int main ( int argc,char* argv[] )
{
  unsigned int command;

  int debug=0;
  int n_vfe=0;
  int vfe[MAX_VFE];
  struct timeval tv;

// CATIA settings if reuested
  int n_catia=0;
  int CATIA_num[5]   ={-1,-1,-1,-1,-1}; // CATIA number to address
  int CATIA_nums     = 12345; // CATIA numbers present on I2C bus (xyzuv) -1=no CATIA)
  int Temp_nums      = 12345; // CATIA number to write in
  int Temp_num[5]    ={-1,-1,-1,-1,-1}; // CATIA number to address
  int Temp_X5        = -1; // Multiply Temp current by factor 5 (=1), no (=0), read both (-1)
  int XADC_reg       =0x11;
  int loop           =0;
  int udelay         =1000000;
  int average        =3;
  unsigned int reg_def[6][7] = {{0x00, 0x02, 0x00, 0x1087, 0xffff, 0xffff, 0x0f},
                                {0x00, 0x02, 0x00, 0x1087, 0xffff, 0xffff, 0x0f},
                                {0x00, 0x02, 0x00, 0x1087, 0xffff, 0xffff, 0x0f},
                                {0x00, 0x02, 0x00, 0x1087, 0xffff, 0xffff, 0x0f},
                                {0x00, 0x02, 0x00, 0x1087, 0xffff, 0xffff, 0x0f},
                                {0x00, 0x02, 0x00, 0x1087, 0xffff, 0xffff, 0x0f}};

  if( argc<2)
  {
    printf("Please type bin/debug_XADC -h to get info on the way to use this program.\n");
    exit(-1);
  }
  for(int k=1; k<argc; k++) 
  {    
    if(strcmp(argv[k],"-vfe") == 0)
    {
      sscanf( argv[++k], "%d", &vfe[n_vfe++] );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_nums") == 0)
    {    
      sscanf( argv[++k], "%d", &CATIA_nums );
      continue;
    }    
    else if(strcmp(argv[k],"-Temp_nums") == 0)
    {    
      sscanf( argv[++k], "%d", &Temp_nums );
      continue;
    }
    else if(strcmp(argv[k],"-XADC_reg") == 0)
    {    
      sscanf( argv[++k], "%d", &XADC_reg );
      continue;
    }    
    else if(strcmp(argv[k],"-Temp_X5") == 0)
    {    
      sscanf( argv[++k], "%d", &Temp_X5 );
      continue;
    }    
    else if(strcmp(argv[k],"-loop") == 0)
    {    
      sscanf( argv[++k], "%d", &loop );
      continue;
    }    
    else if(strcmp(argv[k],"-udelay") == 0)
    {    
      sscanf( argv[++k], "%d", &udelay );
      continue;
    }    
    else if(strcmp(argv[k],"-average") == 0)
    {    
      sscanf( argv[++k], "%d", &average );
      continue;
    }    
    else if(strcmp(argv[k],"-debug") == 0)
    {    
      sscanf( argv[++k], "%d", &debug );
      continue;
    }    
    else if(strcmp(argv[k],"-h") == 0)
    {
      printf("Start DAQ with : \n");
      printf("-vfe                      : FE_adapter board to access [5]\n");
      printf("-CATIA_nums               : CATIAs present on I2C bus which will be read [12334] (-1=no catia)\n");
      printf("-Temp_nums                : CATIAs on which you measure the temperature [12345]\n");
      printf("-XADC_reg                 : XADC register you want to read (CATIA temperature = 17) [17]\n");
      printf("-Temp_X5                  : Program CATIA register 1 to multiply the current by factor 5 (1) or not (0), or read both (-1) [-1]\n");
      printf("-loop                     : Loop on measurements (-1=loop for ever) [0]\n");
      printf("-udelay                   : Delay in us between 2 measurements [1000000]\n");
      printf("-average                  : Ask for average measurements in XADC (0=1,1=16,2=64,3=256) [3]\n");
      exit(-1);
    }
    else
    {
      printf("---------------------------------------------------\n");
      printf("Option %s not defined !, stop here.\n",argv[k]);
      printf("---------------------------------------------------\n");
      exit(-99);
    }
  }
  if(n_vfe==0)
  {
    n_vfe=1;
    vfe[0]=5;
  }

  ConnectionManager manager ( "file:///data/cms/ecal/fe/vfe_catia_v1_ETHZ/software/connection_file.xml" );
  std::vector<uhal::HwInterface> devices;
  int i_vfe;
  for(i_vfe=0; i_vfe<n_vfe; i_vfe++)
  {
    char vice_str[80];
    sprintf(vice_str,"vice.udp.%d",vfe[i_vfe]);
    devices.push_back(manager.getDevice( vice_str ));
  }

  printf("Start DAQ with %d cards : ",n_vfe);
  for(int i=0; i<n_vfe; i++)printf("%d, ",vfe[i]);
  printf("\n");

  n_catia=0;
  while(CATIA_nums>0)
  {
    CATIA_num[n_catia]=CATIA_nums%10;
    CATIA_nums/=10;
    n_catia++;
  }

  int n_temp=0;
  while(Temp_nums>0)
  {
    Temp_num[n_temp]=Temp_nums%10;
    Temp_nums/=10;
    n_temp++;
  }
  printf(" %d catias present : ",n_catia);
  for(int icatia=0; icatia<n_catia; icatia++) printf("%d ",CATIA_num[icatia]);
  printf("\n");
  printf("Will read temperature on %d CATIAs :\n",n_temp);
  for(int itemp=0; itemp<n_temp; itemp++) printf("%d ",Temp_num[itemp]);
  printf("\n");
  printf("Will read XADC register 0x%x\n", XADC_reg);

// Init stage :
  i_vfe=0;
  for(auto & hw : devices)
  {
  // Read FW version to check :
    ValWord<uint32_t> reg = hw.getNode("FW_VER").read();
    hw.dispatch();
    printf("Firmware version      : %8.8x\n",reg.value());
    i_vfe++;
  }

  printf("Reset VFE board\n");
  for(auto & hw : devices)
  {
    hw.getNode("VICE_CTRL").write(RESET*1);
    //hw.getNode("VICE_CTRL").write(RESET*0);
    hw.dispatch();
  }
  usleep(100);

// Tell which CATIA have to send temperature :
  for(auto & hw : devices)
  {
// Read XADC register 0x40 and set the requested average 
    command=DRP_WRb*0 | (0x40<<16);
    hw.getNode("DRP_XADC").write(command);
    ValWord<uint32_t> ave  = hw.getNode("DRP_XADC").read();
    hw.dispatch();
    unsigned loc_ave=ave.value()&0xffff;
    printf("Old config register 0 content : %x\n",loc_ave);

    loc_ave=(loc_ave&0xcfff)|((average&3)<<12);
    command=DRP_WRb*1 | (0x40<<16) | loc_ave;
    hw.getNode("DRP_XADC").write(command);
    hw.dispatch();
    command=DRP_WRb*0 | (0x40<<16);
    hw.getNode("DRP_XADC").write(command);
    ave  = hw.getNode("DRP_XADC").read();
    hw.dispatch();
    loc_ave=ave.value()&0xffff;
    printf("New config register 0 content : %x\n",loc_ave);

// Try to read temperature in Xilinx :
// First ask for data (register 0x10 for APD_temp, 0x11 for CATIA_temp
    FILE *fout=fopen("temp.dat","a+");
    ValWord<uint32_t> reg;
    int I2C_busy=1;
    int n_X5;
    int X5[2];
    if(Temp_X5>=0)
    {
      n_X5=1;
      X5[0]=Temp_X5;
    }
    else
    {
      n_X5=2;
      X5[0]=0;
      X5[1]=1;
    }

    while(loop!=0)
    {
      gettimeofday(&tv,NULL);
      fprintf(fout,"%ld %ld ",tv.tv_sec,tv.tv_usec);
      for(int itemp=0; itemp<n_temp; itemp++)
      {
        for(int iX5=0; iX5<n_X5; iX5++)
        {
          command=I2C_RWb*0 | I2C_LONG*0 | I2C_CATIA*1 | ((Temp_num[itemp]&0xF)<<20) | (1<<16) | (X5[iX5]<<4) | 0xa;
          printf("I2C : set CATIA %d register 1 with data 0xa. Command = 0x%x\n",Temp_num[itemp], command);
          hw.getNode("VFE_CTRL").write(command);
          hw.dispatch();
// Read back what we have put :
          command=I2C_RWb*1 | I2C_LONG*0 | I2C_CATIA*1 | ((Temp_num[itemp]&0xF)<<20) | (1<<16);
          hw.getNode("VFE_CTRL").write(command);
          hw.dispatch();
          // Wait for transaction end :
          I2C_busy=1;
          while(I2C_busy==1)
          {
            reg = hw.getNode("VFE_CTRL").read();
            hw.dispatch();
            I2C_busy=(reg.value()>>26)&1;
          }
          printf("I2C : Read CATIA %d register 1. Command = 0x%x : ",Temp_num[itemp],command);
          printf("Value read : 0x%.8x, should be 0x%.8x\n",reg.value(), (X5[iX5]<<4)|0xa);
// Wait for stabilization of Vtemp : 10 ms ?
          usleep(10000);

          command=DRP_WRb*0 | (XADC_reg<<16);
          hw.getNode("DRP_XADC").write(command);
// Then retreive data
          ValWord<uint32_t> conv  = hw.getNode("CONV_XADC").read();
          ValWord<uint32_t> seq   = hw.getNode("SEQ_XADC").read();
          ValWord<uint32_t> access= hw.getNode("ACCESS_XADC").read();
          ValWord<uint32_t> temp  = hw.getNode("DRP_XADC").read();
          hw.dispatch();
          double loc_val=((temp.value()&0xffff)>>4)/4096.;
          fprintf(fout,"%.5f ",loc_val);
          printf("CATIA %d, X5 %d : temperature through XDAC : register %x, data %d, voltage %.4f V\n",
                 X5[iX5],Temp_num[itemp],(temp.value()>>16), temp.value()&0xffff, loc_val);
          printf("Number of conv/seq/access done : %u %u %u\n",conv.value(),seq.value(),access.value());
        }
      }
      usleep(udelay);
      if(loop>0)loop--;
    }
    fclose(fout);
  }
}

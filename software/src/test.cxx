#include "uhal/uhal.hpp"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>

#include <TApplication.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TH1D.h>
#include <TTree.h>
#include <TFile.h>

#define GENE_TRIGGER (1<<0)
#define GENE_CALIB   (1<<1)
#define GENE_100HZ   (1<<2)
#define LED_ON       (1<<3)
#define CALIB_MODE   (1<<4)

#define RESET                  (1<<0)
#define TRIGGER_MODE           (1<<1)
#define SOFT_TRIGGER           (1<<2)
#define SELF_TRIGGER           (1<<3)
#define CLOCK_PHASE            (1<<4)
#define CLOCK_RESET            (1<<7)
#define SELF_TRIGGER_THRESHOLD (1<<8)
#define SELF_TRIGGER_MASK      (1<<22)
#define BOARD_SN               (1<<28)

#define CAPTURE_START 1
#define CAPTURE_STOP  2
//#define SW_DAQ_DELAY 0x1800 // delay for laser with internal trigger
#define SW_DAQ_DELAY 0x0 // delay for laser with external trigger
#define HW_DAQ_DELAY 100 // Laser with external trigger
#define NSAMPLE_MAX 28672
#define MAX_PAYLOAD 1380
#define MAX_VFE     10
#define MAX_STEP    10000

#define DAC_WRITE       (1<<25)
#define ADC_WRITE       (1<<24)
#define DAC_FULL_RESET  (0xF<<16)
#define DAC_SOFT_RESET  (0x7<<16)
#define DAC_VAL_REG     (0x3<<16)
#define DAC_CTRL_REG    (0x4<<16)

#define ADC_OMODE_REG   (0x14<<16) // ADC register to define Output mode of the ADC
#define ADC_ISPAN_REG   (0x18<<16) // ADC register to define input span of the ADC from 1.383V to 2.087V

using namespace uhal;

int main ( int argc,char* argv[] )
{
  int ngood_event=0;
  int soft_reset, full_reset, command;
  ValWord<uint32_t> address;
  ValWord<uint32_t> data[3*NSAMPLE_MAX];
  //ValVector< uint32_t > mem[256];
  ValVector< uint32_t > block,mem;
  short int event[5][NSAMPLE_MAX];
  double fevent[5][NSAMPLE_MAX];
  double dv=1750./16384.; // 14 bits on 1.75V
  int debug=0;
// Define defaults for laser runs :
  int nevent=1000;
  int nsample=100;
  int trigger_type=0;   // pedestal by default
  int soft_trigger=0;   // external trigger by default 
  int self_trigger=0;   // No self trigger 
  int self_trigger_threshold=0;
  int self_trigger_mask=0x1F; // trig on all channels amplitude
  int n_calib=1;
  int calib_step=-128;
  int calib_level=32768;
  int calib_width=256;
  int calib_delay=0;
  int calib_mode=0;
  int i_vfe,n_vfe=0;
  int vfe[MAX_VFE];
  char cdum, output_file[256];
  int negate_data=1; // Negate the ADC output by default (positive signal from 0 not to disturb people)
  int signed_data=0; // Don't use 2's complement. Use normal binary order (0...16383 full scale)
  int input_span=0; // Use default ADC input span (1.75V) can be tuned from 1.383 V to 2.087 V by steps of 0.022V
                    // this is 2's complemented on 5 bits. So from -16 to +15 -> 0x10..0x1f 0x00 0x01..0x0f -> 16..31 0 1..15
  TProfile *pshape[MAX_STEP][5];
  sprintf(output_file,"");

  for(int k=1; k<argc; k++) 
  {    
    int i=strcmp( argv[k], "-dbg");
    if(i == 0)
    {    
      sscanf( argv[++k], "%d", &debug );
      continue;
    }    
    i=strcmp( argv[k], "-nevt");
    if(i == 0)
    {    
      sscanf( argv[++k], "%d", &nevent );
      continue;
    }    
    i=strcmp(argv[k],"-nsample");
    if(i == 0)
    {    
      sscanf( argv[++k], "%d", &nsample );
      continue;
    }    
    i=strcmp(argv[k],"-soft_trigger");
    if(i == 0)
    {    
// soft_trigger
// 0 : Use external trigger (GPIO)
// 1 : Generate trigger from software (1 written in FW register 
      sscanf( argv[++k], "%d", &soft_trigger );
      continue;
    }    
    i=strcmp(argv[k],"-trigger_type");
    if(i == 0)
    {    
// trigger_type
// 0 : pedestal
// 1 : calibration
// 2 : laser
      sscanf( argv[++k], "%d", &trigger_type );
      continue;
    }
    i=strcmp(argv[k],"-self_trigger");
    if(i == 0)
    {    
// self_trigger
// 0 : Don't generate trigger from data themselves
// 1 : Generate trigger if any data > self_trigger_threshold
      sscanf( argv[++k], "%d", &self_trigger );
      continue;
    }
    i=strcmp(argv[k],"-self_trigger_threshold");
    if(i == 0)
    {    
// self_trigger_threshold in ADC counts
      sscanf( argv[++k], "%d", &self_trigger_threshold );
      continue;
    }
    i=strcmp(argv[k],"-self_trigger_mask");
    if(i == 0)
    {    
// channel mask to generate self trigger lsb=ch0 ... msb=ch4
      sscanf( argv[++k], "%d", &self_trigger_mask );
      continue;
    }
    i=strcmp(argv[k],"-vfe");
    if(i == 0)
    {
      sscanf( argv[++k], "%d", &vfe[n_vfe++] );
      continue;
    }    
    i=strcmp(argv[k],"-calib_mode");
    if(i == 0)
    {    
// Calib mode : 0=external trigger does not generate calibration
//              1=external trigger generate calibration trigger
      sscanf( argv[++k], "%d", &calib_mode );
      continue;
    }    
    i=strcmp(argv[k],"-calib_level");
    if(i == 0)
    {    
// DAC_value 0 ... 65532
      sscanf( argv[++k], "%d", &calib_level );
      continue;
    }    
    i=strcmp(argv[k],"-calib_width");
    if(i == 0)
    {    
// Calib trigger width 0 ... 65532
      sscanf( argv[++k], "%d", &calib_width );
      continue;
    }    
    i=strcmp(argv[k],"-calib_delay");
    if(i == 0)
    {    
// DAQ delay for calib triggers : 0..65532
      sscanf( argv[++k], "%d", &calib_delay );
      continue;
    }    
    i=strcmp(argv[k],"-n_calib");
    if(i == 0)
    {    
// Number of calibration step for linearity study
      sscanf( argv[++k], "%d", &n_calib );
      continue;
    }    
    i=strcmp(argv[k],"-calib_step");
    if(i == 0)
    {    
// DAC step for linearity study
      sscanf( argv[++k], "%d", &calib_step );
      continue;
    }    
    i=strcmp(argv[k],"-negate_data");
    if(i == 0)
    {    
// Put ADC in 2's complement
      sscanf( argv[++k], "%d", &negate_data );
      continue;
    }    
    i=strcmp(argv[k],"-signed_data");
    if(i == 0)
    {    
// Put ADC in 2's complement
      sscanf( argv[++k], "%d", &signed_data );
      continue;
    }    
    i=strcmp(argv[k],"-input_span");
    if(i == 0)
    {    
// Set ADC input SPAN from 0x1f - 0 - 0x0f
      sscanf( argv[++k], "%d", &input_span );
      continue;
    }    
    i=strcmp(argv[k],"-h");
    if(i == 0)
    {
      printf("Start DAQ with : \n");
      printf("-dpg dbg_level            : Set debug level for this session [0]\n");
      printf("-vfe n                    : Do DAQ on VFE n [1000]\n");
      printf("-nevt n                   : Number of events to record  [1000]\n");
      printf("-nsample n                : Number of sample per event (max=28670) [150]\n");
      printf("-trigger type n           : 0=pedestal, 1=calibration, 2=laser [0]\n");
      printf("-soft trigger n           : 0=externally triggered DAQ, 1=softwared triggered DAQ [0]\n");
      printf("-self trigger n           : 1=internal generated trigger if signal > threshold [0]\n");
      printf("-self_trigger_threshold n : minimal signal amplitude to generate self trigger [0]\n");
      printf("-self_trigger_mask n      : channel mask to generate self triggers, ch0=lsb    [0x1F]\n");
      printf("-calib_mode 0/1           : 1=external trigger generates calibration trigger in VFE [0]\n");
      printf("-calib_width n            : width of the calibration trigger sent to VFE [128]\n");
      printf("-calib_delay n            : delay between calibration trigger and DAQ start [0]\n");
      printf("-calib_level n            : DAC level to start linearity stud [32768]\n");
      printf("-n_calib n                : number of calibration steps for linearity study [1]\n");
      printf("-calib_step n             : DAC step for linearity study [128]\n");
      printf("-negate_data 0/1          : Negate (1) or not (0) the converted data in the ADC [0]\n");
      printf("-signed_data 0/1          : Set ADC in normal binary mode (0) or 2's complement (1) [0]\n");
      printf("-input_span n             : Set ADC input span (0x10 = 1.383V, 0x1f=1.727V, 0=1.75V, 0x01=1.772V, 0x0f=2.087V) [0]\n");
      exit(-1);
    }
  }
  if(n_vfe==0)
  {
    n_vfe=1;
    vfe[0]=4;
  }

  //if(trigger_type==0 || trigger_type==1)
  //{
  //  soft_trigger=1;
  //  self_trigger=0;
  //}
  //else if(trigger_type==2)
  //{
  //  soft_trigger=0;
  //  self_trigger=1;
  //  self_trigger_threshold=14000;
  //}
  printf("Start DAQ with %d cards : ",n_vfe);
  for(int i=0; i<n_vfe; i++)printf("%d, ",vfe[i]);
  printf("\n");
  printf("Parameters : \n");
  printf("  %d events \n",nevent);
  printf("  %d samples \n",nsample);
  printf("  trigger type  : %d (0=pedestal, 1=calibration, 2=laser)\n",trigger_type);
  printf("  soft trigger  : %d (0=externally triggered DAQ, 1=softwared triggered DAQ)\n",soft_trigger);
  printf("  self trigger  : %d (1=internal generated trigger if signal > threshold)\n",self_trigger);
  printf("  threshold     : %d (minimal signal amplitude to generate self trigger)\n",self_trigger_threshold);
  printf("  mask          : 0x%x (channel mask to generate self triggers)\n",self_trigger_mask);
  printf("  calib_width   : %d (width of the calibration trigger sent to VFE)\n",calib_width);
  printf("  calib_delay   : %d (delay between calibration trigger and DAQ start)\n",calib_delay);
  printf("  n_calib       : %d (number of calibration steps for linearity study)\n",n_calib);
  printf("  calib_step    : %d (DAC step for linearity study)\n",calib_step);
  if(n_calib<=0)n_calib=1;

  TApplication *Root_App=new TApplication("test", &argc, argv);
  TCanvas *c1=new TCanvas("c1","c1",800.,1000.);
  c1->Divide(2,3);
  TGraph *tg[5];
  TH1D *hmean[5], *hrms[5];
  double rms[5];
  char hname[8];
  for(int ich=0; ich<5; ich++)
  {
    tg[ich] = new TGraph();
    tg[ich]->SetMarkerStyle(20);
    tg[ich]->SetMarkerSize(0.5);
    sprintf(hname,"mean_ch%d",ich);
    hmean[ich]=new TH1D(hname,hname,100,150.,250.);
    sprintf(hname,"rms_ch%d",ich);
    hrms[ich]=new TH1D(hname,hname,200,0.,2.);
  }
  tg[0]->SetLineColor(kRed);
  tg[1]->SetLineColor(kYellow);
  tg[2]->SetLineColor(kBlue);
  tg[3]->SetLineColor(kMagenta);
  tg[4]->SetLineColor(kCyan);
  tg[0]->SetMarkerColor(kRed);
  tg[1]->SetMarkerColor(kYellow);
  tg[2]->SetMarkerColor(kBlue);
  tg[3]->SetMarkerColor(kMagenta);
  tg[4]->SetMarkerColor(kCyan);
  c1->Update();
  int dac_val=calib_level;

  for(int istep=0; istep<n_calib; istep++)
  {
    for(int ich=0; ich<5*n_vfe; ich++)
    {
      char pname[132];
      sprintf(pname,"ch_%d_step_%d_%d",ich,istep,dac_val);
      pshape[istep][ich]=new TProfile(pname,pname,nsample,0.,6.25*nsample);
    }
    dac_val+=calib_step;
  }

  TCanvas *c2=new TCanvas("mean","mean",800.,1000.);
  c2->Divide(2,3);
  c2->Update();
  TCanvas *c3=new TCanvas("rms","rms",800.,1000.);
  c3->Divide(2,3);
  c3->Update();
  printf("RMS : ");
  for(int ich=0; ich<5; ich++)
  {
    c2->cd(ich+1);
    hmean[ich]->Draw();
    c2->Update();
    c3->cd(ich+1);
    hrms[ich]->Draw();
    rms[ich]=hrms[ich]->GetMean();
    printf("%e, ",rms[ich]);
    c3->Update();
  }
  printf("\n");
  time_t now;
  now=time(NULL);
  if(trigger_type==0)
    sprintf(output_file,"/data/cms/ecal/fe/vfe_asic_irrad_top/test/ped_data.%ld.root",now);
  else if(trigger_type==1)
    sprintf(output_file,"/data/cms/ecal/fe/vfe_asic_irrad_top/test/TP_data.%ld.root",now);
  else if(trigger_type==2)
    sprintf(output_file,"/data/cms/ecal/fe/vfe_asic_irrad_top/test/laser_data.%ld.root",now);
  else
    sprintf(output_file,"/data/cms/ecal/fe/vfe_asic_irrad_top/test/vice_data.%ld.root",now);

  //TFile *fd=new TFile(output_file,"recreate");
  printf("output file : xxxx%sxxxx\n",output_file);
  printf("Finished with %d events recorded\n",ngood_event);
}

#include "uhal/uhal.hpp"

#include <vector>
#include <iostream>
#include <cstdlib>
#include <typeinfo>

#include <TApplication.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TH1D.h>
#include <TTree.h>
#include <TFile.h>

#define GENE_TRIGGER    (1<<0)
#define GENE_CALIB      (1<<1)
#define GENE_100HZ      (1<<2)
#define LED_ON          (1<<3)
#define CALIB_MODE      (1<<4)

#define RESET                  (1<<0)
#define TRIGGER_MODE           (1<<1)
#define SOFT_TRIGGER           (1<<2)
#define SELF_TRIGGER           (1<<3)
#define CLOCK_PHASE            (1<<4)
#define CLOCK_RESET            (1<<7)
#define SELF_TRIGGER_THRESHOLD (1<<8)
#define SELF_TRIGGER_MASK      (1<<22)
#define ADC_CALIB_MODE         (1<<27)
#define BOARD_SN               (1<<28)
#define SEQ_CLOCK_PHASE        (1<<0)
#define IO_CLOCK_PHASE         (1<<4)
#define REG_CLOCK_PHASE        (1<<8)
#define MEM_CLOCK_PHASE        (1<<12)

#define CAPTURE_START 1
#define CAPTURE_STOP  2
//#define SW_DAQ_DELAY 0x1800 // delay for laser with internal trigger
#define SW_DAQ_DELAY 0x0 // delay for laser with external trigger
#define HW_DAQ_DELAY 100 // Laser with external trigger
#define NSAMPLE_MAX 28672
#define MAX_PAYLOAD 1380
#define MAX_VFE     10
#define MAX_STEP    10000

#define I2C_RWb         (1<<31)
#define I2C_LONG        (1<<30)
#define SPI_CATIA       (1<<26)
#define I2C_CATIA       (1<<25)
#define ADC_WRITE       (1<<24)
#define DAC_FULL_RESET  (0xF<<16)
#define DAC_SOFT_RESET  (0x7<<16)
#define DAC_VAL_REG     (0x3<<16)
#define DAC_CTRL_REG    (0x4<<16)

#define ADC_OMODE_REG   (0x14<<16) // ADC register to define Output mode of the ADC
#define ADC_ISPAN_REG   (0x18<<16) // ADC register to define input span of the ADC from 1.383V to 2.087V
#define ADC_CLK_DIVIDE  (0x0B<<16) // ADC register to define clock division of the ADC
#define ADC_OTEST_REG   (0x0D<<16) // ADC register to define test mode of the ADC

#define DELAY_INCREASE  (1<<5)     // Increase ADC signal delay by one tip
#define DELAY_RESET     (1<<6)     // Reset ADC signal delays

using namespace uhal;

Int_t main ( Int_t argc,char* argv[] )
{
  Int_t ngood_event=0;
  Int_t soft_reset, full_reset, command;
  ValWord<uint32_t> address;
  ValWord<uint32_t> data[3*NSAMPLE_MAX];
  //ValVector< uint32_t > mem[256];
  ValVector< uint32_t > block,mem;
  short int event[5][NSAMPLE_MAX];
  double fevent[5][NSAMPLE_MAX];
  double dv=1750./16384.; // 14 bits on 1.75V
  Int_t debug=0;
// Define defaults for laser runs :
  Int_t nevent=1000;
  Int_t nsample=100;
  Int_t trigger_type=0;         // pedestal by default
  Int_t soft_trigger=0;         // external trigger by default 
  Int_t self_trigger=0;         // No self trigger 
  Int_t self_trigger_threshold=0;
  Int_t self_trigger_mask=0x1F; // trig on all channels amplitude
  Int_t ADC_calib_mode = 0;     // Put (1) or not (0) the outputs of CATIA in ADC_calib_mode (outputs near VCM)
  Int_t seq_clock_phase    = 0; // sequence clock phase by steps of 45 degrees
  Int_t IO_clock_phase     = 0; // Capture clock phase by steps of 45 degrees
  Int_t reg_clock_phase    = 0; // Shift register clock phase by steps of 45 degrees
  Int_t mem_clock_phase    = 0; // memory clock phase by steps of 45 degrees
  Int_t clock_phase        = 0;
  Int_t n_calib=1;
  Int_t calib_gain=0;
  Int_t calib_step=128;
  Int_t calib_level=0;
  Int_t calib_width=32;
  Int_t calib_delay=50;
  Int_t calib_mode=0;
  Int_t n_vfe=0;
  Int_t vfe[MAX_VFE];
  char cdum, output_file[256];
  Int_t tune_delay=1;
  Int_t delay_val[5]={8,7,4,1,0}; // Number of delay taps to get a stable R/O with ADCs (250 MHz)
  //Int_t delay_val[5]={28,24,20,16,0}; // Number of delay taps to get a stable R/O with ADCs (250 MHz)
  //Int_t delay_val[5]={25,24,20,18,0};     // Number of delay taps to get a stable R/O with ADCs (160 MHz)

// ADC settings if any :
  Int_t ADC_reg=-1;   // ADC register to write in.
  Int_t ADC_data=-1;  // data to write in ADC register.
  Int_t has_ADC=0;        // Put 1 here if you want to read ADCs, if there are any.
  Int_t negate_data=0;    // Don't negate the ADC output by default (positive signal from 0 not to disturb people)
  Int_t signed_data=0;    // Don't use 2's complement. Use normal binary order (0...16383 full scale)
  Int_t input_span=0;     // Use default ADC input span (1.75V) can be tuned from 1.383 V to 2.087 V by steps of 0.022V
                        // this is 2's complemented on 5 bits. So from -16 to +15 -> 0x10..0x1f 0x00 0x01..0x0f -> 16..31 0 1..15
  Int_t output_test=0;    // Put ADC outputs in test mode (0=normal conversion values, 0xF=ramp)
  Int_t clock_divide=0;   // Put ADC clock division (0=take FE clock, 1=divide by 2)

// CATIA settings if reuested
  Int_t n_catia=0;
  Int_t CATIA_num[5]   ={-1,-1,-1,-1,-1}; // CATIA number to address
  Int_t CATIA_nums     =-1; // CATIA number to address (xyzuv) -1=no CATIA)
  Int_t CATIA_data     =-1; // Data to write in CATIA register
  Int_t CATIA_reg      =-1; // Register to write in with SPI or I2C protocol
  Int_t I2C_dir        =-1; // read(1)/write(0) with I2C protocol, -1=don't use I2C
  Int_t SPI_dir        =-1; // read(1)/write(0) with SPI protocol, -1=don't use SPI
  Int_t Reg3_def       = 0x1087; // Default content of Register 3 : 2.5V, LPF35, 400 Ohms, ped mid scale
  Int_t Reg4_def       = 0x1000; // Default content of Register 4 : DAC1 0, DAC1 ON, DAC2 OFF, no copy
  Int_t Reg5_def       = 0x0000; // Default content of Register 5 : DAC2 0
  Int_t Reg6_def       = 0x000b; // Default content of Register 6 : Vreg ON, Rconv=2471, TIA_dummy ON, TP ON

  TProfile *pshape[MAX_STEP][5];
  sprintf(output_file,"");

  for(int k=1; k<argc; k++) 
  {    
    if(strcmp( argv[k], "-dbg") == 0)
    {    
      sscanf( argv[++k], "%d", &debug );
      continue;
    }    
    else if(strcmp( argv[k], "-nevt") == 0)
    {    
      sscanf( argv[++k], "%d", &nevent );
      continue;
    }
    else if(strcmp(argv[k],"-nsample") == 0)
    {    
      sscanf( argv[++k], "%d", &nsample );
      continue;
    }    
    else if(strcmp(argv[k],"-soft_trigger") == 0)
    {    
// soft_trigger
// 0 : Use external trigger (GPIO)
// 1 : Generate trigger from software (1 written in FW register 
      sscanf( argv[++k], "%d", &soft_trigger );
      continue;
    }    
    else if(strcmp(argv[k],"-trigger_type") == 0)
    {    
// trigger_type
// 0 : pedestal
// 1 : calibration
// 2 : laser
      sscanf( argv[++k], "%d", &trigger_type );
      continue;
    }
    else if(strcmp(argv[k],"-self_trigger") == 0)
    {    
// self_trigger
// 0 : Don't generate trigger from data themselves
// 1 : Generate trigger if any data > self_trigger_threshold
      sscanf( argv[++k], "%d", &self_trigger );
      continue;
    }
    else if(strcmp(argv[k],"-self_trigger_threshold") == 0)
    {    
// self_trigger_threshold in ADC counts
      sscanf( argv[++k], "%d", &self_trigger_threshold );
      continue;
    }
    else if(strcmp(argv[k],"-self_trigger_mask") == 0)
    {    
// channel mask to generate self trigger lsb=ch0 ... msb=ch4
      sscanf( argv[++k], "%d", &self_trigger_mask );
      continue;
    }
    else if(strcmp(argv[k],"-vfe") == 0)
    {
      sscanf( argv[++k], "%d", &vfe[n_vfe++] );
      continue;
    }    
    else if(strcmp(argv[k],"-calib_mode") == 0)
    {    
// Calib mode : 0=external trigger does not generate calibration
//              1=external trigger generate calibration trigger
      sscanf( argv[++k], "%d", &calib_mode );
      continue;
    }    
    else if(strcmp(argv[k],"-calib_level") == 0)
    {    
// DAC_value 0 ... 4095
      sscanf( argv[++k], "%d", &calib_level );
      continue;
    }    
    else if(strcmp(argv[k],"-calib_width") == 0)
    {    
// Calib trigger width 0 ... 65532
      sscanf( argv[++k], "%d", &calib_width );
      continue;
    }    
    else if(strcmp(argv[k],"-calib_delay") == 0)
    {    
// DAQ delay for calib triggers : 0..65532
      sscanf( argv[++k], "%d", &calib_delay );
      continue;
    }    
    else if(strcmp(argv[k],"-n_calib") == 0)
    {    
// Number of calibration step for linearity study
      sscanf( argv[++k], "%d", &n_calib );
      continue;
    }    
    else if(strcmp(argv[k],"-calib_step") == 0)
    {    
// DAC step for linearity study
      sscanf( argv[++k], "%d", &calib_step );
      continue;
    }    
    else if(strcmp(argv[k],"-calib_gain") == 0)
    {    
// Rconv for current injection (0=2471 Ohms=G1, 1=272 Ohms=G10)
      sscanf( argv[++k], "%d", &calib_gain );
      continue;
    }    
    else if(strcmp(argv[k],"-negate_data") == 0)
    {    
// Put ADC in invert mode
      sscanf( argv[++k], "%d", &negate_data );
      continue;
    }    
    else if(strcmp(argv[k],"-signed_data") == 0)
    {    
// Put ADC in 2's complement
      sscanf( argv[++k], "%d", &signed_data );
      continue;
    }    
    else if(strcmp(argv[k],"-input_span") == 0)
    {    
// Set ADC input SPAN from 0x1f - 0 - 0x0f
      sscanf( argv[++k], "%d", &input_span );
      continue;
    }    
    else if(strcmp(argv[k],"-output_test") == 0)
    {    
      sscanf( argv[++k], "%x", &output_test );
      continue;
    }    
    else if(strcmp(argv[k],"-clock_divide") == 0)
    {    
      sscanf( argv[++k], "%x", &clock_divide );
      continue;
    }    
    else if(strcmp(argv[k],"-ADC_reg") == 0)
    {    
      sscanf( argv[++k], "%d", &ADC_reg );
      continue;
    }    
    else if(strcmp(argv[k],"-ADC_data") == 0)
    {    
      sscanf( argv[++k], "%d", &ADC_data );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_nums") == 0)
    {    
      sscanf( argv[++k], "%d", &CATIA_nums );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_data") == 0)
    {    
      sscanf( argv[++k], "%x", &CATIA_data );
      continue;
    }    
    else if(strcmp(argv[k],"-CATIA_reg") == 0)
    {    
      sscanf( argv[++k], "%d", &CATIA_reg );
      continue;
    }    
    else if(strcmp(argv[k],"-I2C_dir") == 0)
    {    
      sscanf( argv[++k], "%d", &I2C_dir );
      continue;
    }    
    else if(strcmp(argv[k],"-SPI_dir") == 0)
    {    
      sscanf( argv[++k], "%d", &SPI_dir );
      continue;
    }    
    else if(strcmp(argv[k],"-ADC_calib_mode") == 0)
    {    
      sscanf( argv[++k], "%d", &ADC_calib_mode );
      continue;
    }    
    else if(strcmp(argv[k],"-clock_phase") == 0)
    {    
      sscanf( argv[++k], "%d", &clock_phase );
      seq_clock_phase=(clock_phase>>0)&0x7;
      IO_clock_phase =(clock_phase>>4)&0x7;
      reg_clock_phase=(clock_phase>>8)&0x7;
      mem_clock_phase=(clock_phase>>12)&0x7;
      continue;
    }    
    else if(strcmp(argv[k],"-has_ADC") == 0)
    {    
      sscanf( argv[++k], "%d", &has_ADC );
      continue;
    }    
    else if(strcmp(argv[k],"-Reg3_def") == 0)
    {    
      sscanf( argv[++k], "%x", &Reg3_def );
      continue;
    }    
    else if(strcmp(argv[k],"-Reg4_def") == 0)
    {    
      sscanf( argv[++k], "%x", &Reg4_def );
      continue;
    }    
    else if(strcmp(argv[k],"-Reg5_def") == 0)
    {    
      sscanf( argv[++k], "%x", &Reg5_def );
      continue;
    }    
    else if(strcmp(argv[k],"-Reg6_def") == 0)
    {    
      sscanf( argv[++k], "%x", &Reg6_def );
      continue;
    }    
    else if(strcmp(argv[k],"-h") == 0)
    {
      printf("Start DAQ with : \n");
      printf("-dbg dbg_level            : Set debug level for this session [0]\n");
      printf("-vfe n                    : Do DAQ on VFE n [1000]\n");
      printf("-nevt n                   : Number of events to record  [1000]\n");
      printf("-nsample n                : Number of sample per event (max=28670) [150]\n");
      printf("-trigger type n           : 0=pedestal, 1=calibration, 2=laser [0]\n");
      printf("-soft trigger n           : 0=externally triggered DAQ, 1=softwared triggered DAQ [0]\n");
      printf("-self trigger n           : 1=internal generated trigger if signal > threshold [0]\n");
      printf("-self_trigger_threshold n : minimal signal amplitude to generate self trigger [0]\n");
      printf("-self_trigger_mask n      : channel mask to generate self triggers, ch0=lsb    [0x1F]\n");
      printf("-calib_mode 0/1           : 1=external trigger generates calibration trigger in VFE [0]\n");
      printf("-calib_width n            : width of the calibration trigger sent to VFE [128]\n");
      printf("-calib_delay n            : delay between calibration trigger and DAQ start [0]\n");
      printf("-calib_level n            : DAC level to start linearity study [32768]\n");
      printf("-n_calib n                : number of calibration steps for linearity study [1]\n");
      printf("-calib_step n             : DAC step for linearity study [128]\n");
      printf("-calib_gain n             : Conversion gain for current pulse injection (0=2471 Ohms, 1=272 Ohms) [0]\n");
      printf("-negate_data 0/1          : Negate (1) or not (0) the converted data in the ADC [0]\n");
      printf("-signed_data 0/1          : Set ADC in normal binary mode (0) or 2's complement (1) [0]\n");
      printf("-input_span n             : Set ADC input span (0x10 = 1.383V, 0x1f=1.727V, 0=1.75V, 0x01=1.772V, 0x0f=2.087V) [0]\n");
      printf("-ADC_reg                  : ADC register to write in [-1]\n");
      printf("-ADC_data                 : data to write in ADC register [-1]\n");
      printf("-ADC_calib_mode           : Put (1) or not (0) CATIA in ADC_calib_mode (outputs near VCM) [0]\n");
      printf("-CATIA_nums               : CATIA numbers to address (xyzuv) [-1=no catia]\n");
      printf("-CATIA_data               : data to write in CATIA register [-1]\n");
      printf("-CATIA_reg                : CATIA register to read from/write in with I2C or SPI (1 to 6) [-1]\n");
      printf("                          : 1 : Slow Control reg [R/W]\n");
      printf("                          : 2 : SEU error counter reg [R]\n");
      printf("                          : 3 : TIA reg [R/W]\n");
      printf("                          : 4 : Inj DAC1 reg [R/W]\n");
      printf("                          : 5 : Inj DAC2 reg [R/W]\n");
      printf("                          : 6 : Inj Ctl reg [R/W]\n");
      printf("-Reg3_def                 : default content of register 3 [0x1087]\n");
      printf("-Reg4_def                 : default content of register 4 [0x1000]\n");
      printf("-Reg5_def                 : default content of register 5 [0x0000]\n");
      printf("-Reg6_def                 : default content of register 6 [0x000b]\n");
      printf("-SPI_dir                  : Read (1) or write (0) with SPI protocol. -1=don't use\n");
      printf("-I2C_dir                  : Read (1) or write (0) with I2C protocol. -1=don't use\n");
      printf("-has_ADC n                : Debug analog board (0) or board with ADCs (1) [0]\n");
      exit(-1);
    }
    else
    {
      printf("---------------------------------------------------\n");
      printf("Option %s not defined !, stop here.\n",argv[k]);
      printf("---------------------------------------------------\n");
      exit(-99);
    }
  }
  if(n_vfe==0)
  {
    n_vfe=1;
    vfe[0]=4;
  }
  n_catia=0;
  while(CATIA_nums>0)
  {
    CATIA_num[n_catia]=CATIA_nums%10;
    CATIA_nums/=10;
    n_catia++;
  }
  printf("Will address %d catias : ",n_catia);
  for(Int_t icatia=0; icatia<n_catia; icatia++)
  {
    printf("%d ",CATIA_num[icatia]);
  }
  printf("\n");

  //if(trigger_type==0 || trigger_type==1)
  //{
  //  soft_trigger=1;
  //  self_trigger=0;
  //}
  //else if(trigger_type==2)
  //{
  //  soft_trigger=0;
  //  self_trigger=1;
  //  self_trigger_threshold=14000;
  //}
  printf("Start DAQ with %d cards : ",n_vfe);
  for(int i=0; i<n_vfe; i++)printf("%d, ",vfe[i]);
  printf("\n");
  printf("Parameters : \n");
  if(has_ADC==1)
  {
    printf("Read ADCs for :\n");
    printf("  %d events \n",nevent);
    printf("  %d samples \n",nsample);
    printf("  trigger type  : %d (0=pedestal, 1=calibration, 2=laser)\n",trigger_type);
    printf("  soft trigger  : %d (0=externally triggered DAQ, 1=softwared triggered DAQ)\n",soft_trigger);
    printf("  self trigger  : %d (1=internal generated trigger if signal > threshold)\n",self_trigger);
    printf("  threshold     : %d (minimal signal amplitude to generate self trigger)\n",self_trigger_threshold);
    printf("  mask          : 0x%x (channel mask to generate self triggers)\n",self_trigger_mask);
  }
  if(trigger_type==1)
  {
    printf("Generate calibration triggers :\n");
    printf("  %d events \n",nevent);
    printf("  calib_width   : %d (width of the calibration trigger sent to VFE)\n",calib_width);
    printf("  calib_delay   : %d (delay between calibration trigger and DAQ start)\n",calib_delay);
    printf("  n_calib       : %d (number of calibration steps for linearity study)\n",n_calib);
    printf("  calib_step    : %d (DAC step for linearity study)\n",calib_step);
  }
  if(n_calib==0)n_calib=1;

  int loc_argc=1;
  char *loc_argv[10];
  for(int i=0; i<10; i++)loc_argv[i]=(char *)malloc(132*sizeof(char));
  sprintf(loc_argv[0],"test");
  TApplication *Root_App=new TApplication("test", &loc_argc, loc_argv);
  TCanvas *c1=new TCanvas("c1","c1",1000,0,800.,1000.);
  c1->Divide(2,3);
  TGraph *tg[5];
  TH1D *hmean[5], *hrms[5];
  double rms[5];
  char hname[80];
  for(int ich=0; ich<5; ich++)
  {
    tg[ich] = new TGraph();
    tg[ich]->SetMarkerStyle(20);
    tg[ich]->SetMarkerSize(0.5);
    sprintf(hname,"mean_ch%d",ich);
    hmean[ich]=new TH1D(hname,hname,100,150.,250.);
    sprintf(hname,"rms_ch%d",ich);
    hrms[ich]=new TH1D(hname,hname,200,0.,2.);
  }
  tg[0]->SetLineColor(kRed);
  tg[1]->SetLineColor(kYellow);
  tg[2]->SetLineColor(kBlue);
  tg[3]->SetLineColor(kMagenta);
  tg[4]->SetLineColor(kCyan);
  tg[0]->SetMarkerColor(kRed);
  tg[1]->SetMarkerColor(kYellow);
  tg[2]->SetMarkerColor(kBlue);
  tg[3]->SetMarkerColor(kMagenta);
  tg[4]->SetMarkerColor(kCyan);
  c1->Update();
  int dac_val=calib_level;

  for(int istep=0; istep<n_calib; istep++)
  {
    for(int ich=0; ich<5*n_vfe; ich++)
    {
      char pname[132];
      sprintf(pname,"ch_%d_step_%d_%d",ich,istep,dac_val);
      pshape[istep][ich]=new TProfile(pname,pname,nsample,0.,6.25*nsample);
    }
    dac_val+=calib_step;
  }

  ConnectionManager manager ( "file:///data/cms/ecal/fe/vfe_test_catia_v1/firmware/connection_file.xml" );
  std::vector<uhal::HwInterface> devices;
  int i_vfe;
  for(i_vfe=0; i_vfe<n_vfe; i_vfe++)
  {
    char vice_str[80];
    sprintf(vice_str,"vice.udp.%d",vfe[i_vfe]);
    devices.push_back(manager.getDevice( vice_str ));
  }

  ValWord<uint32_t> free_mem;
  ValWord<uint32_t> trig_reg;
  ValWord<uint32_t> delays;
  int old_address[MAX_VFE];

// Reset board
//  for(int i=0; i<99999999999; i++)
//  {
  //while(1)
  //{
  for(auto & hw : devices)
  {
    hw.getNode("VICE_CTRL").write(RESET*1);
    //hw.getNode("VICE_CTRL").write(RESET*0);
    hw.dispatch();
  }
  usleep(500);
  //usleep(50);
  //}
  
// Program ADCs
  for(auto & hw : devices)
  {
// Put ADC in two's complement mode (if no pedestal bias) and invert de conversion result
    negate_data=(negate_data&1)<<2;
    signed_data&=3;
    command=ADC_WRITE | ADC_OMODE_REG | negate_data | signed_data;
    printf("Put ADC coding : 0x%x\n",command);
    hw.getNode("VFE_CTRL").write(command);
    hw.dispatch();

// Set ADC input range (default=1.75V)
    input_span&=0x1F;
    command=ADC_WRITE | ADC_ISPAN_REG | input_span;
    printf("Set ADC input span range : 0x%x\n",command);
    hw.getNode("VFE_CTRL").write(command);
    hw.dispatch();

// Set ADC output test mode
    output_test&=0x0F;
    command=ADC_WRITE | ADC_OTEST_REG | output_test;
    printf("Set ADC output test mode : 0x%x\n",command);
    hw.getNode("VFE_CTRL").write(command);
    hw.dispatch();

// Divide clock by 2.
    clock_divide&=0x0F;
    command=ADC_WRITE | ADC_CLK_DIVIDE | clock_divide;
    printf("Set ADC clk division to : 0x%x\n",command);
    hw.getNode("VFE_CTRL").write(command);
    hw.dispatch();
  }

// Calibration trigger setting :
  for(auto & hw : devices)
  {
    command=(calib_delay<<16) | (calib_width&0xffff);
    printf("Calibration trigger with %d clocks width and %d clocks delay : %x\n",calib_width,calib_delay,command);
    hw.getNode("CALIB_CTRL").write(command);
    hw.dispatch();
  }

// Set delay tap value for each ADC
  for(auto & hw : devices)
  {
    command=DELAY_RESET*1;
    hw.getNode("DELAY_CTRL").write(command);
    hw.dispatch();
    printf("Get lock status of IDELAY input stages\n");
    ValWord<uint32_t> reg = hw.getNode("DELAY_CTRL").read();
    hw.dispatch();
    printf("Delay lock value : 0x%x\n",reg.value());

    for(int iADC=0; iADC<4; iADC++)
    {
      command=DELAY_RESET*0 | DELAY_INCREASE*1 | (1<<iADC);
      for(Int_t itap=0; itap<delay_val[iADC]; itap++)
      {
        hw.getNode("DELAY_CTRL").write(command);
        hw.dispatch();
      }
      printf("Get lock status of IDELAY input stages\n");
      ValWord<uint32_t> reg = hw.getNode("DELAY_CTRL").read();
      hw.dispatch();
      printf("Value read : 0x%x\n",reg.value());
    }
  }

// Init stage :
  i_vfe=0;
  for(auto & hw : devices)
  {
  // Read FW version to check :
    ValWord<uint32_t> reg = hw.getNode("FW_VER").read();
  // Switch to triggered mode + external trigger :
    command=  (ADC_CALIB_MODE        *(ADC_calib_mode&1))              |
              (SELF_TRIGGER_MASK     *(self_trigger_mask&0x1F))        |
              (SELF_TRIGGER_THRESHOLD*(self_trigger_threshold&0x3FFF)) |
              //(CLOCK_PHASE           *(clock_phase&0x7))               |
               SELF_TRIGGER          *self_trigger                     |
               SOFT_TRIGGER          *soft_trigger                     |
               TRIGGER_MODE          *1                                | // Always DAQ on trigger
               RESET                 *0;
    hw.getNode("VICE_CTRL").write(command);
  // Stop DAQ and ask for NSAMPLE per frame (+timestamp) :
    command = ((nsample+1)<<16)+CAPTURE_STOP;
    hw.getNode("CAP_CTRL").write(command);
  // Add laser latency before catching data ~ 40 us
    hw.getNode("TRIG_DELAY").write((SW_DAQ_DELAY<<16)+HW_DAQ_DELAY);
  // Switch off FE-adapter LEDs
    command = CALIB_MODE*0+LED_ON*0+GENE_100HZ*0+GENE_CALIB*0+GENE_TRIGGER*0;
    hw.getNode("FW_VER").write(command);
    hw.dispatch();
  // Set the clock phases
    command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
    hw.getNode("CLK_SETTING").write(command);
    hw.dispatch();

  // Reset the reading base address :
    hw.getNode("CAP_ADDRESS").write(0);
  // Start DAQ :
    command = ((nsample+1)<<16)+CAPTURE_START;
    hw.getNode("CAP_CTRL").write(command);
  // Read back delay values :
    delays=hw.getNode("TRIG_DELAY").read();
  // Read back the read/write base address
    address = hw.getNode("CAP_ADDRESS").read();
    free_mem = hw.getNode("CAP_FREE").read();
    trig_reg = hw.getNode("VICE_CTRL").read();
    hw.dispatch();

    printf("Firmware version      : %8.8x\n",reg.value());
    printf("Delays                : %8.8x\n",delays.value());
    printf("Initial R/W addresses : 0x%8.8x\n", address.value());
    printf("Free memory           : 0x%8.8x\n", free_mem.value());
    printf("Trigger mode          : 0x%8.8x\n", trig_reg.value());
    old_address[i_vfe]=address>>16;
    if(old_address[i_vfe]==0x6fff)old_address[i_vfe]=-1;
    i_vfe++;
  }

  int n_word=(nsample+1)*3; // 3*32 bits words per sample to get the 5 channels adta
  int n_transfer=n_word/(MAX_PAYLOAD/4); // Max ethernet packet = 1536 bytes, max user payload = 1500 bytes
  int n_last=n_word-n_transfer*(MAX_PAYLOAD/4);
  printf("Reading events by blocks of %dx32b-words, %d bits\n",n_word, n_word*32);
  printf("Using %d transfers of %d words + 1 transfer of %d words\n",n_transfer, MAX_PAYLOAD/4,n_last);
  if(n_transfer > 249)
  {
    printf("Event size too big ! Please reduce number of samples per frame.\n");
    printf("Max frame size : 28670\n");
  }

  unsigned long int timestamp=0;
  TTree *tdata=new TTree("data","data");
  tdata->Branch("timestamp",&timestamp,"timestamp/l");
  for(int ich=0; ich<5; ich++)
  {
    char bname[80], btype[80];
    sprintf(bname,"ch%d",ich);
    sprintf(btype,"ch%d[%d]/S",ich,nsample);
    tdata->Branch(bname,event[ich],btype);
  }

// Program CATIA according to wishes :
// With SPI protocol
  if(SPI_dir>=0)
  {
    for(auto & hw : devices)
    {
      for(Int_t icatia=0; icatia<n_catia; icatia++)
      {
        unsigned int loc_reg=0;
    // Address to decode on the VFE board (0..3 are used for the ADCs)
        if(CATIA_reg==1)loc_reg=8;
        if(CATIA_reg==3)loc_reg=4;
        if(CATIA_reg==4)loc_reg=5;
        if(CATIA_reg==5)loc_reg=6;
        if(CATIA_reg==6)loc_reg=7;
        loc_reg+=(CATIA_num[icatia]-1)*5;
    // Write CATIA register with SPI protocol (no readback)
        output_test= (loc_reg<<16) | (CATIA_data & 0xFFFF);
        command=I2C_RWb*0 | SPI_CATIA*1 | output_test;
        printf("SPI : set CATIA %d register %d with data %d. Command = 0x%x\n",CATIA_num[icatia], CATIA_reg,CATIA_data,command);
        hw.getNode("VFE_CTRL").write(command);
        hw.dispatch();
      }
    }
  }

// Setup default values for CATIA:
  unsigned int loc_reg=0;
  unsigned int I2C_long=0;
  for(auto & hw : devices)
  {
    for(Int_t icatia=0; icatia<n_catia; icatia++)
    {
      loc_reg=(CATIA_num[icatia]<<4) | 3;
// Gain 400 Ohm, Output stage for 2.5V, LPF35, 0 pedestal
      output_test= (loc_reg<<16) | Reg3_def;
      printf("Put Reg3 content to 0x%x : 0x%x\n",Reg3_def,output_test);
      command=I2C_RWb*0 | I2C_LONG*1 | I2C_CATIA*1 | output_test;
      hw.getNode("VFE_CTRL").write(command);
      hw.dispatch();

      loc_reg=(CATIA_num[icatia]<<4) | 4;
// DAC1 0, DAC2 0, DAC1 ON, DAC2 OFF, DAC1 copied on DAC2
      output_test= (loc_reg<<16) | Reg4_def;
      printf("Put Reg4 content to 0x%x : 0x%x\n",Reg4_def,output_test);
      command=I2C_RWb*0 | I2C_LONG*1 | I2C_CATIA*1 | output_test;
      hw.getNode("VFE_CTRL").write(command);
      hw.dispatch();

      loc_reg=(CATIA_num[icatia]<<4) | 5;
// DAC2 mid scale but OFF, so should not matter
      //output_test= (loc_reg<<16) | 0xffff;
// DAC2 at 0 but OFF, so should not matter
      printf("Put Reg5 content to 0x%x : 0x%x\n",Reg5_def,output_test);
      output_test= (loc_reg<<16) | Reg5_def;
      command=I2C_RWb*0 | I2C_LONG*1 | I2C_CATIA*1 | output_test;
      hw.getNode("VFE_CTRL").write(command);
      hw.dispatch();

      loc_reg=(CATIA_num[icatia]<<4) | 6;
// TIA dummy ON, Rconv 2471 (G10 scale), Vref ON (0xb) OFF (0x3), Injection in CATIA
      output_test= (loc_reg<<16) | Reg6_def;
      printf("Put Reg6 content to 0x%x : 0x%x\n",Reg6_def,output_test);
      command=I2C_RWb*0 | I2C_LONG*0 | I2C_CATIA*1 | output_test;
      hw.getNode("VFE_CTRL").write(command);
      hw.dispatch();
      //
// Custom I2C settings
      if(I2C_dir>=0)
      {
        printf("Access CATIA with I2C :\n");
        for(auto & hw : devices)
        {
          I2C_long=0;
          if(CATIA_reg==3 || CATIA_reg==4 || CATIA_reg==5)I2C_long=1;

          if(I2C_dir==1) // Read
          {
      // I2C address : 0xyz000, with xyz=001 (CATIA_num=1) or 010 (CATIA_num=2), shifted left by 3 in firmware 
            loc_reg=((CATIA_num[icatia]&0x0F)<<4) | (CATIA_reg&0x0F);
            output_test= (loc_reg<<16) | (CATIA_data & 0xFFFF);
            command=I2C_RWb*1 | I2C_LONG*I2C_long | I2C_CATIA*1 | output_test;
            printf("I2C : Read CATIA %d register %d. Command = 0x%x\n",CATIA_num[icatia], CATIA_reg,command);
            hw.getNode("VFE_CTRL").write(command);
            hw.dispatch();
            usleep(10000);
            printf("Get I2C register content with IPBus\n");
            ValWord<uint32_t> reg = hw.getNode("VFE_CTRL").read();
            hw.dispatch();
            printf("Value read : 0x%x\n",reg.value());
          }
          else           // Write
          {
      // I2C address : 0xyz000, with xyz=001 (CATIA_num=1) or 010 (CATIA_num=2), shifted left by 3 in firmware 
            loc_reg=((CATIA_num[icatia]&0x0F)<<4) | (CATIA_reg&0x0F);
            output_test= (loc_reg<<16) | (CATIA_data & 0xFFFF);
            command=I2C_RWb*0 | I2C_LONG*I2C_long | I2C_CATIA*1 | output_test;
            printf("I2C : set CATIA %d register %d with data %d. Command = 0x%x\n",CATIA_num[icatia], CATIA_reg,CATIA_data,command);
            hw.getNode("VFE_CTRL").write(command);
            hw.dispatch();
            command=I2C_RWb*1 | I2C_LONG*I2C_long | I2C_CATIA*1 | output_test;
            printf("I2C : Read back CATIA %d register %d. Command = 0x%x\n",CATIA_num[icatia], CATIA_reg,command);
            hw.getNode("VFE_CTRL").write(command);
            hw.dispatch();
            usleep(10000);
            printf("Get I2C register content with IPBus\n");
            ValWord<uint32_t> reg = hw.getNode("VFE_CTRL").read();
            hw.dispatch();
            printf("Value read : 0x%x\n",reg.value());
          }
        }
      }
    }
  }
  if(n_calib<0)exit(-1);
  printf("Should not go there\n");

// Send triggers and wait between each trigger :
  for(int istep=0; istep<n_calib; istep++)
  {
// Program DAC for this step
    for(auto & hw : devices)
    {
      for(Int_t icatia=0; icatia<n_catia; icatia++)
      {
        if(calib_level>=0)
        {
          loc_reg=(CATIA_num[icatia]<<4) | 4; // Register 4 of CATIA = DAC1 level
          output_test= (loc_reg<<16) | 0x1000 | (calib_level&0x0fff); // Use only DAC1 output
          command=I2C_RWb*0 | I2C_LONG*1 | I2C_CATIA*1 | output_test;
          printf("Put %d in DAC register : 0x%x\n",calib_level,command);
          hw.getNode("VFE_CTRL").write(command);
          hw.dispatch();
          command=I2C_RWb*1 | I2C_LONG*1 | I2C_CATIA*1 | output_test;
          printf("I2C : Read back CATIA %d register %d. Command = 0x%x\n",CATIA_num[icatia], CATIA_reg,command);
          hw.getNode("VFE_CTRL").write(command);
          hw.dispatch();
          printf("Get I2C register content with IPBus\n");
          ValWord<uint32_t> reg = hw.getNode("VFE_CTRL").read();
          hw.dispatch();
          printf("Value read : 0x%x\n",reg.value()&0xffff);
// Wait for Vdac to stabilize :
          usleep(1000000);
        }
        else
        {
  // switch off all the injection system
          loc_reg=(CATIA_num[icatia]<<4) | 6;
          output_test= (loc_reg<<16) | 0x0;
          command=I2C_RWb*0 | I2C_LONG*1 | I2C_CATIA*1 | output_test;
          hw.getNode("VFE_CTRL").write(command);
          hw.dispatch();
        }
      }
    }
    int ievt=0;
    printf("start sending calibration triggers :\n"); 
    while(ievt<nevent)
    {
      if((ievt%100)==0)printf("%d\n",ievt); 
      i_vfe=0;
      for(auto & hw : devices)
      {
        if(debug>0)
        {
          command = ((nsample+1)<<16)+CAPTURE_START;
          hw.getNode("CAP_CTRL").write(command);
          hw.dispatch();
        }
        if(soft_trigger == 1)
        {
          if(trigger_type == 0)
            command = CALIB_MODE*0+LED_ON*0+GENE_100HZ*0+GENE_CALIB*0+GENE_TRIGGER*1;
          else if(trigger_type == 1)
            command = CALIB_MODE*0+LED_ON*0+GENE_100HZ*0+GENE_CALIB*1+GENE_TRIGGER*0;

          free_mem = hw.getNode("CAP_FREE").read();
          hw.getNode("FW_VER").write(command);
          hw.dispatch();
          if(has_ADC==1)
          {
            address = hw.getNode("CAP_ADDRESS").read();
            hw.dispatch();
            int new_address=address.value()>>16;
            int loc_address=new_address;
            int nretry=0;
            while(((loc_address-old_address[i_vfe])%28672) != nsample+1 && nretry<100)
            {
              address = hw.getNode("CAP_ADDRESS").read();
              hw.dispatch();
              new_address=address.value()>>16;
              loc_address=new_address;
              if(new_address<old_address[i_vfe])loc_address+=28672;
              nretry++;
              //printf("ongoing R/W addresses    : old %d, new %d add 0x%8.8x\n", old_address[i_vfe], new_address,address.value());
            }
            if(nretry==100)
            {
              printf("Stop waiting for sample capture after %d retries\n",nretry);
              printf("R/W addresses    : old %8.8x, new %8.8x add 0x%8.8x\n", old_address[i_vfe], new_address, address.value());
            }
            old_address[i_vfe]=new_address;
            if(old_address[i_vfe]==0x6fff)old_address[i_vfe]=-1;
            //printf("R/W addresses    : old %8.8x, new %8.8x add 0x%8.8x\n", old_address[i_vfe], new_address, address.value());
          }
          else
          {
            usleep(10000);
          }
        }
        else if(has_ADC==1)
        {
          free_mem = hw.getNode("CAP_FREE").read();
          address = hw.getNode("CAP_ADDRESS").read();
          hw.dispatch();
          //printf("address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
          if(debug>0)printf("address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
          if(free_mem.value()==28671)continue;
        }
        ievt++;

// Read event samples from FPGA
        if(has_ADC==1)
        {
          mem.clear();
          for(int itrans=0; itrans<n_transfer; itrans++)
          {
            block = hw.getNode ("CAP_DATA").readBlock(MAX_PAYLOAD/4);
            hw.dispatch();
            for(int is=0; is<MAX_PAYLOAD/4; is++)mem.push_back(block[is]);
          }
          block = hw.getNode ("CAP_DATA").readBlock(n_last);
          address = hw.getNode("CAP_ADDRESS").read();
          free_mem = hw.getNode("CAP_FREE").read();
          hw.dispatch();
          if(debug>0)printf("After reading address : 0x%8.8x, Free memory : %d\n",address.value(),free_mem.value());
          for(int is=0; is<n_last; is++)mem.push_back(block[is]);
          mem.valid(true);

          double ped[5], ave[5], rms[5];
          for(int ich=0; ich<5; ich++)
          {
            ped[ich]=0.;
            ave[ich]=0.;
            rms[ich]=0.;
          }
          double max=0.;
      // First sample should have bit 70 at 1
          if((mem[0]>>31) != 1) printf("Sample 0 not a header : %8.8x\n",mem[0]);
          unsigned long int t1= mem[0]     &0xFFFF;
          unsigned long int t2= mem[1]     &0xFFFF;
          unsigned long int t3=(mem[1]>>16)&0xFFFF;
          unsigned long int t4= mem[2]     &0xFFFF;
          unsigned long int t5=(mem[2]>>16)&0x00FF;
          timestamp=(t5<<56)+(t4<<42)+(t3<<28)+(t2<<14)+t1;
          
          if(debug>0)
          {
            printf("timestamp : %8.8x %8.8x %8.8x\n",mem[2],mem[1],mem[0]);
            printf("timestamp : %ld %4.4lx %4.4lx %4.4lx %4.4lx %4.4lx\n",timestamp,t5,t4,t3,t2,t1);
          }
          for(int isample=0; isample<nsample; isample++)
          {
            int j=(isample+1)*3;
  // If data are coded in 2's complement, propagate sign bit (b13) to bit 14 and 15 of short int array 
  // to keep signed data
            event[0][isample]= mem[j]       &0xFFFF;
            if(signed_data==1 && ((event[0][isample]>>13)&1)==1)event[0][isample]|=0xc000;
            event[1][isample]= mem[j+1]     &0xFFFF;
            if(signed_data==1 && ((event[1][isample]>>13)&1)==1)event[1][isample]|=0xc000;
            event[2][isample]=(mem[j+1]>>16)&0xFFFF;
            if(signed_data==1 && ((event[2][isample]>>13)&1)==1)event[2][isample]|=0xc000;
            event[3][isample]= mem[j+2]     &0xFFFF;
            if(signed_data==1 && ((event[3][isample]>>13)&1)==1)event[3][isample]|=0xc000;
            event[4][isample]=(mem[j+2]>>16)&0xFFFF;
            if(signed_data==1 && ((event[4][isample]>>13)&1)==1)event[4][isample]|=0xc000;
            if(signed_data==1)
            {
              //event[0][isample]-=8192;
              //event[1][isample]-=8192;
              //event[2][isample]-=8192;
              //event[3][isample]-=8192;
              //event[4][isample]-=8192;
            }
            //if(debug>0)printf("%8.8x %8.8x %8.8x\n",mem[j],mem[j+1],mem[j+2]);
  // With Catia, signal is negative, based at ADC positive rail
  // Converted to user friendly data in FPGA.
            fevent[0][isample]=double(event[0][isample]);
            fevent[1][isample]=(double)event[1][isample];
            fevent[2][isample]=(double)event[2][isample];
            fevent[3][isample]=(double)event[3][isample];
            fevent[4][isample]=(double)event[4][isample];

            if(dv*event[1][isample]>max)max=dv*event[1][isample];
          }

          for(int ich=0; ich<5; ich++)
          {
            for(int isample=0; isample<nsample; isample++)
            {
              //tg[ich]->SetPoint(isample,6.25*isample,dv*event[ich][isample]);
              tg[ich]->SetPoint(isample,6.25*isample,fevent[ich][isample]);
              ave[ich]+=dv*fevent[ich][isample];
              if(isample<30)ped[ich]+=dv*fevent[ich][isample];
              rms[ich]+=dv*fevent[ich][isample]*dv*fevent[ich][isample];
              pshape[istep][i_vfe*5+ich]->Fill(6.25*isample+1.,dv*fevent[ich][isample]);
            }
          }
          for(int ich=0; ich<5; ich++)
          {
            ave[ich]/=nsample;
            ped[ich]/=30.;
            rms[ich]/=nsample;
            rms[ich]=sqrt(rms[ich]-ave[ich]*ave[ich]);
            if(debug>0)printf("ich %d : ped=%f, ave=%f, rms=%f\n",ich,ped[ich],ave[ich],rms[ich]);
            hmean[ich]->Fill(ave[ich]);
            hrms[ich]->Fill(rms[ich]);
          }
          if(trigger_type==0 || max>0.)
          {
            tdata->Fill();
            if((ngood_event%200)==0)printf("%d events recorded\n",ngood_event);
            ngood_event++;
          }
          //if(debug>0 && max>0.)
          if(debug>0 || (ievt%100)==1)
          {
            c1->cd(1);
            tg[0]->Draw("alp");
            c1->cd(2);
            tg[1]->Draw("alp");
            c1->cd(3);
            tg[2]->Draw("alp");
            c1->cd(4);
            tg[3]->Draw("alp");
            c1->cd(5);
            tg[4]->Draw("alp");
            c1->Update();
            if(debug==1)
            {
              Int_t increase_delay=-1;
              Int_t delay_reset=0;
              Int_t set_clock=0, increase_seq_clock=0, increase_IO_clock=0, increase_reg_clock=0, increase_mem_clock=0;
              printf("Press any key to continue :\n");
              printf("0/z : reset line delays\n");
              printf("1<n<5 : tune ADC n line delays\n");
              printf("+ : increase delay of ADC n lines\n");
              printf("- : decrease delay of ADC n lines\n");
              printf("s : increase sequence clock phase by 45 deg (%d)\n",seq_clock_phase);
              printf("i : increase IO clock phase by 45 deg (%d)\n",IO_clock_phase);
              printf("r : increase register clock phase by 45 deg (%d)\n",reg_clock_phase);
              printf("m : increase memory clock phase by 45 deg (%d)\n",mem_clock_phase);

              system("stty raw");
              cdum=getchar();
              system("stty -raw");
              while (cdum=='1' || cdum=='2' ||cdum=='3' ||cdum=='4' ||cdum=='5')
              {
                if(cdum=='1')tune_delay=1<<0;
                if(cdum=='2')tune_delay=1<<1;
                if(cdum=='3')tune_delay=1<<2;
                if(cdum=='4')tune_delay=1<<3;
                if(cdum=='5')tune_delay=1<<4;
                system("stty raw");
                cdum=getchar();
                system("stty -raw");
              }
              if(cdum=='+'|| cdum=='=') increase_delay=1;
              if(cdum=='-') increase_delay=0;
              if(cdum=='0' || cdum=='z') delay_reset=1;
              if(cdum=='s'|| cdum=='S') {set_clock=1; increase_seq_clock=1;}
              if(cdum=='i'|| cdum=='I') {set_clock=1; increase_IO_clock=1;}
              if(cdum=='r'|| cdum=='R') {set_clock=1; increase_reg_clock=1;}
              if(cdum=='m'|| cdum=='M') {set_clock=1; increase_mem_clock=1;}
              if(cdum=='q' || cdum=='Q' || cdum=='0') continue;
              printf("Tune delay for ADC %d ",tune_delay);
              printf("in direction %d (0=down, 1=up, -1=skip)\n",increase_delay);
              if(increase_delay>=0)
              {
                command=DELAY_RESET*0 | DELAY_INCREASE*increase_delay | tune_delay;
                hw.getNode("DELAY_CTRL").write(command);
                hw.dispatch();
                printf("Get lock status of IDELAY input stages\n");
                ValWord<uint32_t> reg = hw.getNode("DELAY_CTRL").read();
                hw.dispatch();
                printf("Value read : 0x%x\n",reg.value());
              }
              if(delay_reset==1)
              {
                command=DELAY_RESET*delay_reset;
                hw.getNode("DELAY_CTRL").write(command);
                hw.dispatch();
                printf("Get lock status of IDELAY input stages\n");
                ValWord<uint32_t> reg = hw.getNode("DELAY_CTRL").read();
                hw.dispatch();
                printf("Value read : 0x%x\n",reg.value());
              }
              if(set_clock==1)
              {
                if(increase_seq_clock==1)seq_clock_phase=(seq_clock_phase+1)%8;
                if(increase_IO_clock==1) IO_clock_phase=(IO_clock_phase+1)%8;
                if(increase_reg_clock==1)reg_clock_phase=(reg_clock_phase+1)%8;
                if(increase_mem_clock==1)mem_clock_phase=(mem_clock_phase+1)%8;
                command = MEM_CLOCK_PHASE*mem_clock_phase+REG_CLOCK_PHASE*reg_clock_phase+IO_CLOCK_PHASE*IO_clock_phase+SEQ_CLOCK_PHASE*seq_clock_phase;
                hw.getNode("CLK_SETTING").write(command);
              }
            }
            if(debug==2)usleep(100000);
            if(cdum=='q' || cdum=='Q') break;
          }
          if(debug>0)
          {
            command = ((nsample+1)<<16)+CAPTURE_STOP;
            hw.getNode("CAP_CTRL").write(command);
            hw.dispatch();
            old_address[i_vfe]=-1;
          }
          i_vfe++;
        }
        if(cdum=='q' || cdum=='Q') break;
      }
    }
    calib_level+=calib_step;
  }

  for(auto & hw : devices)
  {
// Stop DAQ :
    command = ((nsample+1)<<16)+CAPTURE_STOP;
    hw.getNode("CAP_CTRL").write(command);
  // Switch on FE-adapter LEDs
    command = LED_ON*1+GENE_100HZ*0+GENE_TRIGGER*0;
    hw.getNode("FW_VER").write(command);
    hw.dispatch();
  }

  if(has_ADC==1)
  {
    TCanvas *c2=new TCanvas("mean","mean",800.,1000.);
    c2->Divide(2,3);
    c2->Update();
    TCanvas *c3=new TCanvas("rms","rms",800.,1000.);
    c3->Divide(2,3);
    c3->Update();
    printf("RMS : ");
    for(int ich=0; ich<5; ich++)
    {
      c2->cd(ich+1);
      hmean[ich]->Draw();
      c2->Update();
      c3->cd(ich+1);
      hrms[ich]->Draw();
      rms[ich]=hrms[ich]->GetMean();
      printf("%e, ",rms[ich]);
      c3->Update();
    }
    printf("\n");
    if(trigger_type==0)
      sprintf(output_file,"data/ped_data.root");
    else if(trigger_type==1)
      sprintf(output_file,"data/TP_data.root");
    else if(trigger_type==2)
      sprintf(output_file,"data/laser_data.root");
    else
      sprintf(output_file,"data/vice_data.root");

    TFile *fd=new TFile(output_file,"recreate");
    tdata->Write();
    c1->Write();
    c2->Write();
    c3->Write();
    for(int istep=0; istep<n_calib; istep++)
    {
      for(i_vfe=0; i_vfe<n_vfe; i_vfe++)
      {
        for(int ich=0; ich<5; ich++)
        {
          pshape[istep][i_vfe*5+ich]->Write();
        }
      }
    }
    fd->Close();
    printf("Finished with %d events recorded\n",ngood_event);
  }
}

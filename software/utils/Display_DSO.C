#include <stdio.h>
#include <stdlib.h>
#include "TPad.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TProfile.h"
#include "TVirtualFFT.h"
void Display_DSO(TString fname1="", Double_t off1=0., TString fname2="", Double_t off2=0., 
                 TString fname3="", Double_t off3=0., TString fname4="", Double_t off4=0.,
                 Double_t xmin=-99999., Double_t xmax=-99999., Double_t ymin=-99999., Double_t ymax=-99999.,
                 TString legend1="", TString legend2="", TString legend3="", TString legend4="",
                 Double_t probe_att1=1., Double_t probe_att2=1.,Double_t probe_att3=1.,Double_t probe_att4=1.)
{
  Double_t att[4];
  att[0]=probe_att1;
  att[1]=probe_att2;
  att[2]=probe_att3;
  att[3]=probe_att4;
  char fname[132];
  FILE *fd[4]={NULL,NULL,NULL,NULL};
  char *line=NULL;
  size_t len;
  TGraph *tg[6];
  tg[0]=new TGraph();
  tg[1]=new TGraph();
  tg[2]=new TGraph();
  tg[3]=new TGraph();
  tg[4]=new TGraph();
  tg[5]=new TGraph();
  Int_t color[6]={kRed, kBlue, kGreen, kOrange+1, kGreen, kBlue};
  Double_t offset[4];
  offset[0]=off1;
  Int_t compute_diff1=0;
  Int_t compute_diff2=0;
  if(off2>=1000.){compute_diff1=1; off2-=1000.;}
  if(off4>=1000.){compute_diff2=1; off4-=1000.;}
  offset[1]=off2;
  offset[2]=off3;
  offset[3]=off4;

  TCanvas *c0=new TCanvas("event","event");

  printf("Opening file %s\n",fname1.Data());
  fd[0]=fopen(fname1.Data(),"r");
  if(fd[0]==NULL)
  {
    printf("Unable to open file %s. Should not happen. Stop here\n",fname1.Data());
     return;
  }
  printf("Opening file %s\n",fname2.Data());
  fd[1]=fopen(fname2.Data(),"r");
  if(fd[1]==NULL)
  {
    printf("Unable to open file %s. Skip it. Analyse only one file\n",fname2.Data());
  }
  printf("Opening file %s\n",fname3.Data());
  fd[2]=fopen(fname3.Data(),"r");
  if(fd[2]==NULL)
  {
    printf("Unable to open file %s. Skip it.\n",fname3.Data());
  }
  printf("Opening file %s\n",fname4.Data());
  fd[3]=fopen(fname4.Data(),"r");
  if(fd[3]==NULL)
  {
    printf("Unable to open file %s. Skip it.\n",fname4.Data());
  }

  c0->cd();
  Int_t n=0;
  for(int ifile=0; ifile<4; ifile++)
  {
    if(fd[ifile]==NULL)continue;
// First track :
// skip 4 lines at beginning of each file
    int eof=getline(&line, &len, fd[ifile]);
    eof=getline(&line, &len, fd[ifile]);
    eof=getline(&line, &len, fd[ifile]);
    eof=getline(&line, &len, fd[ifile]);
    eof=getline(&line, &len, fd[ifile]);
    n=0;
    while((eof=getline(&line, &len, fd[ifile])) != EOF)
    {
      Double_t t,y=0.;
      int iret=sscanf(line,"%lf %lf",&t,&y);
      tg[ifile]->SetPoint(n++,t*1.e9,(y+offset[ifile])*1000*att[ifile]);
    }
    fclose(fd[ifile]);
    printf("track %d read : %d points\n",ifile,n);
    tg[ifile]->SetMarkerStyle(20);
    tg[ifile]->SetMarkerSize(0.5);
    tg[ifile]->SetMarkerColor(color[ifile]);
    tg[ifile]->SetLineColor(color[ifile]);
    if(ifile==0)
    {
      if(ymin>-99990.)tg[ifile]->SetMinimum(ymin);
      if(ymax>-99990.)tg[ifile]->SetMaximum(ymax);
      tg[ifile]->Draw("alp");
      if(xmin>-99990. && xmax>-99990.)tg[ifile]->GetXaxis()->SetLimits(xmin,xmax);
      gPad->SetGridx();
      gPad->SetGridy();
      tg[ifile]->GetXaxis()->SetTitle("time [ns]");
      tg[ifile]->GetXaxis()->SetTitleFont(62);
      tg[ifile]->GetXaxis()->SetTitleSize(0.05);
      tg[ifile]->GetXaxis()->SetLabelFont(62);
      tg[ifile]->GetXaxis()->SetLabelSize(0.05);
      tg[ifile]->GetYaxis()->SetTitle("amplitude [mV]");
      tg[ifile]->GetYaxis()->SetTitleFont(62);
      tg[ifile]->GetYaxis()->SetTitleSize(0.05);
      tg[ifile]->GetYaxis()->SetLabelFont(62);
      tg[ifile]->GetYaxis()->SetLabelSize(0.05);
      tg[ifile]->GetYaxis()->SetTitleOffset(1.00);
    }
    else
      tg[ifile]->Draw("lp");
  }
  TCanvas *c1;
  if(compute_diff1==1)
  {
    c1=new TCanvas("diff","diff");
    for(Int_t i=0; i<n; i++)
    {
      Double_t t,y1,y2;
      tg[0]->GetPoint(i,t,y1);
      tg[1]->GetPoint(i,t,y2);
      tg[4]->SetPoint(i,t,(y1-offset[0])-(y2-offset[1]));
    }
    tg[4]->SetMarkerStyle(20);
    tg[4]->SetMarkerSize(0.5);
    tg[4]->SetMarkerColor(color[4]);
    tg[4]->SetLineColor(color[4]);
    tg[4]->SetMinimum(-(ymax-ymin)/20.);
    if(ymax>-99990.)tg[4]->SetMaximum(ymax-ymin);
    tg[4]->Draw("alp");
    if(xmin>-99990. && xmax>-99990.)tg[4]->GetXaxis()->SetLimits(xmin,xmax);
    gPad->SetGridx();
    gPad->SetGridy();
    tg[4]->GetXaxis()->SetTitle("time [ns]");
    tg[4]->GetXaxis()->SetTitleFont(62);
    tg[4]->GetXaxis()->SetTitleSize(0.05);
    tg[4]->GetXaxis()->SetLabelFont(62);
    tg[4]->GetXaxis()->SetLabelSize(0.05);
    tg[4]->GetYaxis()->SetTitle("amplitude [mV]");
    tg[4]->GetYaxis()->SetTitleFont(62);
    tg[4]->GetYaxis()->SetTitleSize(0.05);
    tg[4]->GetYaxis()->SetLabelFont(62);
    tg[4]->GetYaxis()->SetLabelSize(0.05);
    tg[4]->GetYaxis()->SetTitleOffset(1.00);
    printf("Diffwerential signal RMS : %e %e\n",tg[4]->GetRMS(1), tg[4]->GetRMS(2)); 
    c1->Update();
  }
  if(compute_diff2==1)
  {
    for(Int_t i=0; i<n; i++)
    {
      Double_t t,y1,y2;
      tg[2]->GetPoint(i,t,y1);
      tg[3]->GetPoint(i,t,y2);
      tg[5]->SetPoint(i,t,(y1-offset[0])-(y2-offset[1]));
    }
    c1->cd();
    tg[5]->SetMarkerStyle(20);
    tg[5]->SetMarkerSize(0.5);
    tg[5]->SetMarkerColor(color[5]);
    tg[5]->SetLineColor(color[5]);
    tg[5]->SetMinimum(-(ymax-ymin)/20.);
    tg[5]->Draw("lp");
    printf("Diffwerential signal RMS : %e %e\n",tg[5]->GetRMS(1), tg[5]->GetRMS(2)); 
    c1->Update();
  }
  TLegend *tl=new TLegend();
  tl->SetFillStyle(0);
  if(strcmp(legend1.Data(),"")!=0)tl->AddEntry(tg[0],legend1.Data(),"lp");
  if(strcmp(legend2.Data(),"")!=0)tl->AddEntry(tg[1],legend2.Data(),"lp");
  if(strcmp(legend3.Data(),"")!=0)tl->AddEntry(tg[2],legend3.Data(),"lp");
  if(strcmp(legend4.Data(),"")!=0)tl->AddEntry(tg[3],legend4.Data(),"lp");
  tl->Draw();

  c0->Update();
}

